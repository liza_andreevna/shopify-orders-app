import React from "react";
import { ApolloProvider } from "@apollo/client";
import App from "next/app";
import { AppProvider } from "@shopify/polaris";
import { Provider } from "@shopify/app-bridge-react";
import "@shopify/polaris/dist/styles.css";
import translations from "@shopify/polaris/locales/en.json";
import theme from "../src/helpers/settings";
import RoutePropagator from "../src/components/RoutePropagator";
import client from "./apollo-client";
import CustomLink from "../src/components/CustomLink";
import "./styles.scss";

function MyProvider(props) {
  const Component = props.Component;

  return (
    <ApolloProvider client={client}>
      <Component {...props} />
    </ApolloProvider>
  );
}

class MyApp extends App {
  render() {
    const { Component, pageProps, shopOrigin } = this.props;
    return (
      <AppProvider i18n={translations} linkComponent={CustomLink} theme={theme}>
        <Provider
          config={{
            apiKey: API_KEY,
            shopOrigin,
            host: Buffer.from(HOST_URL).toString("base64"),
            forceRedirect: false,
          }}
        >
          <RoutePropagator />
          <MyProvider
            shopOrigin={shopOrigin}
            Component={Component}
            {...pageProps}
          />
        </Provider>
      </AppProvider>
    );
  }
}

MyApp.getInitialProps = async ({ ctx }) => {
  return {
    shopOrigin: ctx.query.shop,
  };
};

export default MyApp;
