import { ApolloClient, HttpLink, InMemoryCache } from "@apollo/client";

const appGraphqlLink = new HttpLink({
  uri: `${process.env.NEXT_PUBLIC_HOST}/graphql`,
  //fetch : authenticatedFetch(app)
  headers: {
    "Content-Type": "application/graphql",
  },
  fetchOptions: {
    credentials: "include",
  },
});
const client = new ApolloClient({
  link: appGraphqlLink,
  cache: new InMemoryCache(),
});

export default client;
