import "@babel/polyfill";
import dotenv from "dotenv";
import Koa from "koa";
import cors from "@koa/cors";
import next from "next";
import Router from "koa-router";
import createShopifyAuth, { verifyRequest } from "@shopify/koa-shopify-auth";
import Shopify, { ApiVersion } from "@shopify/shopify-api";

dotenv.config();
const port = parseInt(process.env.PORT, 10) || 8081;
const dev = process.env.NODE_ENV !== "production";
const app = next({
  dev,
});
const handle = app.getRequestHandler();

Shopify.Context.initialize({
  API_KEY: process.env.SHOPIFY_API_KEY,
  API_SECRET_KEY: process.env.SHOPIFY_API_SECRET,
  SCOPES: process.env.SCOPES.split(","),
  HOST_NAME: process.env.HOST.replace(/https:\/\/|\/$/g, ""),
  API_VERSION: ApiVersion.Unstable,
  IS_EMBEDDED_APP: false,
  SESSION_STORAGE: new Shopify.Session.MemorySessionStorage(),
});

// Storing the currently active shops in memory will force them to re-login when your server restarts. You should
// persist this object in your app.
export const ACTIVE_SHOPIFY_SHOPS = {};

app.prepare().then(async () => {
  const server = new Koa();
  const router = new Router();
  server.keys = [Shopify.Context.API_SECRET_KEY];

  const options = {
    origin: "*",
  };

  server.use(cors(options));

  server.use(
    createShopifyAuth({
      async afterAuth(ctx) {
        // Access token and shop available in ctx.state.shopify
        const { shop, accessToken, scope } = ctx.state.shopify;
        const host = ctx.query.host;
        ACTIVE_SHOPIFY_SHOPS[shop] = scope;

        const response = await Shopify.Webhooks.Registry.register({
          shop,
          accessToken,
          path: "/webhooks",
          topic: "APP_UNINSTALLED",
          webhookHandler: async (topic, shop, body) =>
            delete ACTIVE_SHOPIFY_SHOPS[shop],
        });

        if (!response.success) {
          console.log(
            `Failed to register APP_UNINSTALLED webhook: ${response.result}`
          );
        }

        // Redirect to app with shop parameter upon auth
        ctx.redirect(`/?shop=${shop}&host=${host}`);
      },
    })
  );

  const handleRequest = async (ctx) => {
    await handle(ctx.req, ctx.res);
    ctx.respond = false;
    ctx.res.statusCode = 200;
  };

  router.get(
    "/",
    verifyRequest({ accessMode: "online", fallbackRoute: "/login" }),
    async (ctx) => {
      const session = await Shopify.Utils.loadCurrentSession(
        ctx.req,
        ctx.res,
        true
      );
      const shop = ctx?.query?.shop?.toString() || session.shop;

      if (session && session.isOnline) {
        await handleRequest(ctx);
      } else {
        return ctx.redirect(`/auth?shop=${shop}`);
      }
    }
  );

  router.get("/login", async (ctx) => {
    const session = await Shopify.Utils.loadCurrentSession(
      ctx.req,
      ctx.res,
      true
    );

    if (session && session.isOnline && session.expires) {
      return ctx.redirect(`/?shop=${session.shop}`);
    }
    await handleRequest(ctx);
  });

  router.post(
    "/graphql",
    verifyRequest({ accessMode: "online", returnHeader: true }),
    async (ctx, next) => {
      await Shopify.Utils.graphqlProxy(ctx.req, ctx.res);
    }
  );

  router.get("/api/statistic", async (ctx) => {
    try {
      const session = await Shopify.Utils.loadCurrentSession(ctx.req, ctx.res);
      const client = new Shopify.Clients.Rest(
        session.shop,
        session.accessToken
      );
      const data = await client.get({
        path: "orders/count",
        query: {
          status: "any",
          created_at_min: `${ctx.query.created_at_min}`,
          created_at_max: `${ctx.query.created_at_max}`,
        },
      });
      ctx.body = data;
      ctx.status = 200;
    } catch (err) {
      ctx.status = err.statusCode || err.status || 500;
      ctx.body = {
        message: err.message,
      };
    }
  });

  router.get("(/_next/static/.*)", handleRequest); // Static content is clear
  router.get("/_next/webpack-hmr", handleRequest); // Webpack content is clear

  router.get("(/_next/image)", handleRequest);

  router.get(
    "(.*)",
    verifyRequest({ accessMode: "online", fallbackRoute: "/login" }),
    handleRequest
  );

  server.use(router.allowedMethods());
  server.use(router.routes());
  server.listen(port, () => {
    console.log(`> Ready on http://localhost:${port}`);
  });
});
