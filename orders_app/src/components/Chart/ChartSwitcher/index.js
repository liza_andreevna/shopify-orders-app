import React from "react";
import styles from "./styles.module.scss";
import line from "../../icons/LineChartIcon";
import column from "../../icons/ColumnChartIcon";
import { ButtonGroup, Button } from "@shopify/polaris";

const ChartSwitcher = ({ name, onChange }) => {
  return (
    <div className={styles.ChartTypeToggle}>
      <ButtonGroup segmented>
        {["line", "column"].map((btn) => (
          <Button
            primary={btn === name}
            key={btn}
            onClick={() => onChange(btn)}
            icon={btn == "line" ? line : column}
            outline={btn !== name}
          />
        ))}
      </ButtonGroup>
    </div>
  );
};

export default ChartSwitcher;
