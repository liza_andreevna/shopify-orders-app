import { getDays } from "../../../helpers/getDays";
import { fetchAction } from "../../../helpers/fetchCount";

export const OrdersCount = async () => {
  const days = getDays();
  const OrdersDate = [];
  for (let i = 0; i < days.length; i++) {
    const params = {
      created_at_min: days[i],
      created_at_max: days[i + 1] || days[i],
    };
    const response = await fetchAction(params);
    OrdersDate.push([days[i], response.data.body.count]);
  }
  return OrdersDate;
};
