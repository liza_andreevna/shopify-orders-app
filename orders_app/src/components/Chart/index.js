import React, { createElement, useEffect, useState } from "react";
import { OrdersCount } from "./FecthAction";
import styles from "./styles.module.scss";
import { SkeletonDisplayText, Spinner } from "@shopify/polaris";
import "chartkick/chart.js";
import { LineChart, ColumnChart } from "react-chartkick";
import ChartSwitcher from "./ChartSwitcher";

const chart = {
  line: LineChart,
  column: ColumnChart,
};

const ChartView = ({ title, type }) => {
  const [data, setData] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [activeToggleType, setActiveToggleType] = useState(type);
  useEffect(() => {
    if (!sessionStorage.getItem("orders-count")) {
      OrdersCount().then((v) => {
        sessionStorage.setItem("orders-count", JSON.stringify(v));
        setData(v);
        setIsLoading(false);
      });
    } else {
      setData(JSON.parse(sessionStorage.getItem("orders-count")));
      setIsLoading(false);
    }
  }, []);
  const typeChangeHandler = () =>
    setActiveToggleType((currentType) =>
      currentType === "line" ? "column" : "line"
    );
  const sortByDate = (a, b) => {
    return new Date(a[0]) - new Date(b[0]);
  };
  return (
    <div className={styles.Chart}>
      <div className={styles.optionsWrapper}>
        <div className={styles.Chart__title}>
          {isLoading ? <SkeletonDisplayText size="medium" /> : title}
        </div>
        <div className={styles.Chart__switcher}>
          {!isLoading && (
            <ChartSwitcher
              name={activeToggleType}
              onChange={typeChangeHandler}
            />
          )}
        </div>
      </div>
      {!isLoading ? (
        chart.hasOwnProperty(activeToggleType) &&
        createElement(chart[activeToggleType], {
          data: activeToggleType == "line" ? data : data.sort(sortByDate),
          height: "235px",
        })
      ) : (
        <Spinner accessibilityLabel="Spinner example" size="large" />
      )}
    </div>
  );
};

export default ChartView;
