import React from "react";
import Link from "next/link";

const CustomLink = ({ url, children, ...props }) => {
  return (
    <Link href={url} passHref>
      <a {...props}>{children}</a>
    </Link>
  );
};

export default CustomLink;
