import React from "react";
import { Card, Link, TextStyle } from "@shopify/polaris";
import styles from "./styles.module.scss";

const CustomerCard = ({ customer, shippingAddress, router }) => {
  return (
    <Card title="Customer">
      <Card.Section>
        <Link
          url={`https://${
            router.query.shop
          }/admin/customers/${customer.id.substr(23)}`}
        >
          {customer.displayName}
        </Link>
      </Card.Section>
      <Card.Section title="Contact Information">
        <div className={styles.contactWrapper}>
          <Link url={`mailto:${customer.email}`}>{customer.email}</Link>
          <TextStyle variation="subdued">
            {customer.phone !== null ? `${customer.phone}` : "No phone number"}
          </TextStyle>
        </div>
      </Card.Section>
      <Card.Section title="Shipping Address">
        <p>{shippingAddress.name}</p>
        <p>{shippingAddress.address1}</p>
        <p>{shippingAddress.city}</p>
        <p>{shippingAddress.zip}</p>
        <p>{shippingAddress.country}</p>
      </Card.Section>
      <Card.Section title="Billing Address">
        <TextStyle variation="subdued">Same as shipping address</TextStyle>
      </Card.Section>
    </Card>
  );
};

export default CustomerCard;
