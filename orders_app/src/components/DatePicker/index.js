import React, { useCallback, useState } from "react";
import { Button, Popover, DatePicker } from "@shopify/polaris";
import { CalendarMinor } from "@shopify/polaris-icons";
import styles from "./styles.module.scss";
import { getMonth } from "date-fns";

const DatePickerView = ({ onChange, date }) => {
  const [datePopoverActive, setDatePopoverActive] = useState(false);
  const toggleDatePickerActive = useCallback(
    () => setDatePopoverActive((popoverActive) => !popoverActive),
    []
  );
  const [{ month, year }, setDate] = useState({
    month: getMonth(date.start),
    year: new Date().getFullYear() - 1,
  });

  const handleMonthChange = useCallback(
    (month, year) => setDate({ month, year }),
    []
  );

  const datePickerActivator = (
    <div className={styles.DatePicker__activator}>
      <Button onClick={toggleDatePickerActive} icon={CalendarMinor}>
        Last 30 days
      </Button>
    </div>
  );

  return (
    <div className={styles.DatePicker}>
      <Popover
        active={datePopoverActive}
        activator={datePickerActivator}
        onClose={toggleDatePickerActive}
      >
        <div className={styles.DatePicker__content}>
          <DatePicker
            onMonthChange={handleMonthChange}
            onChange={onChange}
            selected={date}
            month={month}
            year={year}
            multiMonth
            allowRange
          />
        </div>
      </Popover>
    </div>
  );
};

export default DatePickerView;
