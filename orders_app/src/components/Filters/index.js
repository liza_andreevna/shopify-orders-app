import React, { useEffect, useState } from "react";
import { Icon, TextField } from "@shopify/polaris";
import { SearchMinor } from "@shopify/polaris-icons";
import styles from "./styles.module.scss";
import DatePickerView from "../DatePicker";
import SortingPicker from "../SortingPicker";
import { useDebounce } from "../../helpers/useDebounce";

const SearchFilters = ({
  onSearch,
  onSortingStatus,
  onDateFilter,
  selectedDates,
}) => {
  const [search, setSearch] = useState("");
  const debouncedSearch = useDebounce(search, 700);
  useEffect(() => {
    onSearch(debouncedSearch.trim());
  }, [debouncedSearch]);
  return (
    <div className={styles.FiltersWrapper}>
      <div className={styles.FiltersWrapper__search}>
        <TextField
          type="search"
          value={search}
          onChange={(value) => setSearch(value)}
          prefix={<Icon source={SearchMinor} />}
          placeholder="Search here"
          autoComplete="off"
        />
      </div>
      <div className={styles.FiltersWrapper__sortingPicker}>
        <SortingPicker onChange={onSortingStatus} />
      </div>
      <div className={styles.FiltersWrapper__datePicker}>
        <DatePickerView date={selectedDates} onChange={onDateFilter} />
      </div>
    </div>
  );
};

export default SearchFilters;
