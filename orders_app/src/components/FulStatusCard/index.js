import React from "react";
import {
  Card,
  Icon,
  List,
  Thumbnail,
  Badge,
  Link,
  Stack,
} from "@shopify/polaris";
import { FUL_LABELS_STATUS } from "../../constants";
import { MobileHorizontalDotsMajor } from "@shopify/polaris-icons";
import styles from "./styles.module.scss";

const FulStatusCard = ({ order, router }) => {
  const lineItems = order?.lineItems?.edges;
  return (
    <Card
      title={`${FUL_LABELS_STATUS[order.displayFulfillmentStatus]} (${
        lineItems.length
      })`}
      actions={[
        {
          content: <Icon source={MobileHorizontalDotsMajor} />,
        },
      ]}
    >
      <Card.Section>
        <div className={styles.listWrapper}>
          <List>
            {lineItems.map((item) => (
              <List.Item key={item.node.id}>
                <Stack>
                  <Stack.Item fill>
                    <div className={styles.lineItem}>
                      <div className={styles.imageWrapper}>
                        <Badge>{item.node.quantity}</Badge>
                        <Thumbnail
                          source={`${item.node.image.url}`}
                          alt="Black choker necklace"
                        />
                      </div>
                      <Link
                        url={`https://${
                          router.query.shop
                        }/admin/products/${item.node.product.id.substr(22)}`}
                      >
                        {item.node.title}
                      </Link>
                    </div>
                  </Stack.Item>
                  <Stack.Item>
                    <span className={styles.unit_qty}>
                      {
                        item.node.originalUnitPriceSet.presentmentMoney
                          .currencyCode
                      }{" "}
                      {item.node.originalUnitPriceSet.presentmentMoney.amount} ×{" "}
                      {item.node.quantity}
                    </span>
                  </Stack.Item>
                  <Stack.Item>
                    <span className={styles.qty}>
                      {item.node.originalTotalSet.presentmentMoney.currencyCode}{" "}
                      {item.node.originalTotalSet.presentmentMoney.amount}
                    </span>
                  </Stack.Item>
                </Stack>
              </List.Item>
            ))}
          </List>
        </div>
      </Card.Section>
    </Card>
  );
};

export default FulStatusCard;
