import React from "react";
import { Badge } from "@shopify/polaris";
import { FUL_LABELS_STATUS, FUL_STATUS_BADGE } from "../../constants";
import styles from "./styles.module.scss";

const FulfillmentBadge = ({ status }) => {
  return (
    <div className={styles.bagdeWrapper}>
      <Badge progress={FUL_STATUS_BADGE[status]}>
        {FUL_LABELS_STATUS[status]}
      </Badge>
    </div>
  );
};

export default FulfillmentBadge;
