import React, { useCallback, useState } from "react";
import styles from "./styles.module.scss";
import { Button, Form, FormLayout, TextField } from "@shopify/polaris";

const LoginForm = () => {
  const [error, setError] = useState("");
  const [url, setUrl] = useState("");

  const pattern = /[^.\s]+\.myshopify\.com/;

  const onSubmit = (value) => {
    if (event.target.value == "") {
      return setError("Shop url is required!");
    }

    if (!event.target.url.value.match(pattern)) {
      return setError("Shop url must be demo-store.myshopify.com");
    }

    window.location.href = `/?shop=${event.target.url.value}`;
  };

  const handleUrlChange = useCallback((value) => {
    setError("");
    setUrl(value);
  }, []);
  return (
    <div className={styles.FormWrapper}>
      <Form noValidate action="/" onSubmit={onSubmit}>
        <FormLayout>
          <TextField
            autoComplete="on"
            value={url}
            onChange={handleUrlChange}
            type="url"
            name="url"
            error={error}
            placeholder="demo-store.myshopify.com"
          />
          <Button primary submit>
            Login
          </Button>
        </FormLayout>
      </Form>
    </div>
  );
};

export default LoginForm;
