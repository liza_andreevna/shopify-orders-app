import React, { useMemo } from "react";
import { DisplayText } from "@shopify/polaris";

const MainTitle = () => {
  const changeTitle = useMemo(() => {
    const time = new Date().getHours();
    switch (true) {
      case time > 17:
        return "Good Evening";
      case time > 12:
        return "Good Afternoon ";
      case time > 6:
        return "Good Morning";
      default:
        return "Welcome... but isn't it bed time now?;";
    }
  }, []);

  return <DisplayText size="large">{changeTitle} Elizaveta 👋</DisplayText>;
};

export default MainTitle;
