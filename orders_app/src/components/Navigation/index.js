import { Navigation } from "@shopify/polaris";
import { HomeMajor, OrdersMajor } from "@shopify/polaris-icons";
import styles from "./styles.module.scss";
import { useRouter } from "next/router";

const NavigationMarkup = () => {
  const router = useRouter();
  const { shop, host } = router.query;
  return (
    <div className={styles.NavigationWrapper}>
      <Navigation location={router.asPath}>
        <Navigation.Section
          items={[
            {
              url: `/?shop=${shop}&host=${host}`,
              label: "Home",
              icon: HomeMajor,
              exactMatch: true,
            },
            {
              url: `/orders?shop=${shop}&host=${host}`,
              label: "Orders",
              icon: OrdersMajor,
              selected: !router.asPath.includes("order") ? false : true,
            },
          ]}
        />
      </Navigation>
    </div>
  );
};

export default NavigationMarkup;
