import React from "react";
import { Card } from "@shopify/polaris";

const NoteCard = ({ note }) => {
  return (
    <Card title="Notes" sectioned>
      {note !== null ? <p>{note}</p> : <p>No notes from customer</p>}
    </Card>
  );
};

export default NoteCard;
