import React, { useCallback, useState } from "react";
import { DataTable, EmptyState } from "@shopify/polaris";
import styles from "./styles.module.scss";
import { sortData } from "../../helpers/sortData";

const OrdersTable = ({ initiallySortedRows }) => {
  const [sortRows, setSortRows] = useState([]);
  const rows = initiallySortedRows;
  const handleSort = useCallback(
    (index, direction) => setSortRows(sortData(rows, index, direction)),
    [rows]
  );
  return (
    <div className={styles.OrdersTable}>
      {rows.length > 0 ? (
        <DataTable
          columnContentTypes={[
            "text",
            "numeric",
            "numeric",
            "text",
            "text",
            "text",
          ]}
          headings={[
            "Order",
            "Total Price",
            "Items Count",
            "Address",
            "Created Date",
            "Financial Status",
          ]}
          verticalAlign="middle"
          rows={rows}
          sortable={[false, true, true, false, true, false]}
          defaultSortDirection="descending"
          initialSortColumnIndex={4}
          onSort={handleSort}
        />
      ) : (
        <EmptyState heading="No orders" />
      )}
    </div>
  );
};

export default OrdersTable;
