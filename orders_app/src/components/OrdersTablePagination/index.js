import React, { useEffect, useState } from "react";
import { Button, ButtonGroup } from "@shopify/polaris";
import { ArrowLeftMinor, ArrowRightMinor } from "@shopify/polaris-icons";
import styles from "./styles.module.scss";
import { fetchAction } from "../../helpers/fetchCount";

const OrdersPagination = ({
  createdAtMin,
  createdAtMax,
  hasPrev,
  hasNext,
  onPaginationNext,
  onPaginationPrev,
  ordersCount,
  loading,
}) => {
  const [count, setCount] = useState(0);
  const params = {
    created_at_min: createdAtMin,
    created_at_max: createdAtMax,
  };
  useEffect(() => {
    fetchAction(params)
      .then((res) => {
        setCount(res.data.body.count);
      })
      .catch((err) => console.log(err));
  });
  return (
    <div className={styles.OrdersPagination}>
      <div className={styles.OrdersPagination__buttons}>
        <ButtonGroup segmented>
          <Button
            icon={ArrowLeftMinor}
            onClick={() => onPaginationPrev()}
            disabled={!hasPrev || loading.left}
          />

          <Button
            icon={ArrowRightMinor}
            onClick={() => onPaginationNext()}
            disabled={!hasNext || loading.right}
          />
        </ButtonGroup>
      </div>
      <span className={styles.OrdersPagination__count}>
        Showing {ordersCount} of {count} results
      </span>
    </div>
  );
};

export default OrdersPagination;
