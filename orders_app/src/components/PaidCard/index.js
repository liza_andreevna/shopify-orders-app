import React from "react";
import { Card, Stack } from "@shopify/polaris";
import { STATUS_LABELS } from "../../constants";
import styles from "./styles.module.scss";

const PaidCard = ({ order }) => {
  return (
    <Card title={`${STATUS_LABELS[order.displayFinancialStatus]}`}>
      <Card.Section>
        <div className={styles.stackWrapper}>
          <Card.Subsection>
            <Stack distribution="fill">
              <Stack.Item>
                <span>Subtotal</span>
              </Stack.Item>
              <Stack.Item>
                <span>{order.currentSubtotalLineItemsQuantity} item</span>
              </Stack.Item>
              <Stack.Item fill>
                <span>
                  {order.currentSubtotalPriceSet.presentmentMoney.currencyCode}{" "}
                  {order.currentSubtotalPriceSet.presentmentMoney.amount}
                </span>
              </Stack.Item>
            </Stack>
            <Stack distribution="fill">
              <Stack.Item>
                <span>Shipping</span>
              </Stack.Item>
              <Stack.Item>
                <span>{order.shippingLine.title}</span>
              </Stack.Item>
              <Stack.Item fill>
                <span>
                  {
                    order.shippingLine.originalPriceSet.presentmentMoney
                      .currencyCode
                  }{" "}
                  {order.shippingLine.originalPriceSet.presentmentMoney.amount}
                </span>
              </Stack.Item>
            </Stack>
            {order.taxLines.map((item) => (
              <Stack distribution="fill" key={item.ratePercentage.toString()}>
                <Stack.Item>
                  <span>Tax</span>
                </Stack.Item>
                <Stack.Item>
                  <span>
                    {item.title} {item.ratePercentage}% (included)
                  </span>
                </Stack.Item>
                <Stack.Item fill>
                  <span>
                    {item.priceSet.presentmentMoney.currencyCode}{" "}
                    {item.priceSet.presentmentMoney.amount}
                  </span>
                </Stack.Item>
              </Stack>
            ))}
            <Stack distribution="fill">
              <Stack.Item>
                <span className={styles.strong}>Total</span>
              </Stack.Item>
              <Stack.Item fill>
                <span className={styles.strong}>
                  {order.totalPriceSet.presentmentMoney.currencyCode}{" "}
                  {order.totalPriceSet.presentmentMoney.amount}
                </span>
              </Stack.Item>
            </Stack>
          </Card.Subsection>
          <Card.Subsection>
            {order.transactions.map((item) => (
              <Stack distribution="fill" key={item.id}>
                <Stack.Item>
                  <span>Paid by customer</span>
                </Stack.Item>
                <Stack.Item fill>
                  <span>
                    {item.amountSet.presentmentMoney.currencyCode}{" "}
                    {item.amountSet.presentmentMoney.amount}
                  </span>
                </Stack.Item>
              </Stack>
            ))}
          </Card.Subsection>
        </div>
      </Card.Section>
    </Card>
  );
};

export default PaidCard;
