import React from "react";
import { Badge } from "@shopify/polaris";
import { STATUS_LABELS, STATUS_BADGE } from "../../constants";

const PaymentBadge = ({ status }) => {
  return <Badge progress={STATUS_BADGE[status]}>{STATUS_LABELS[status]}</Badge>;
};

export default PaymentBadge;
