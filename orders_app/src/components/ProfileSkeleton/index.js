import React from "react";
import {
  SkeletonDisplayText,
  SkeletonThumbnail,
  Stack,
  TextContainer,
  SkeletonBodyText,
} from "@shopify/polaris";
import styles from "./styles.module.scss";

const ProfileSkeleton = () => {
  return (
    <div className={styles.ProfileSkeleton}>
      <Stack alignment="center" spacing="tight" distribution="equalSpacing">
        <SkeletonThumbnail size="small" />
        <div className={styles.ProfileSkeleton__text}>
          <TextContainer>
            <SkeletonDisplayText size="small" />
            <SkeletonBodyText lines={1} />
          </TextContainer>
        </div>
      </Stack>
    </div>
  );
};

export default ProfileSkeleton;
