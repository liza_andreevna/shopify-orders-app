import React, { useCallback, useMemo, useState } from "react";
import { ActionList, Button, Popover } from "@shopify/polaris";
import styles from "./styles.module.scss";
import { OrdersMajor } from "@shopify/polaris-icons";
import { STATUS_LABELS, status } from "../../constants";

const SortingStatusPicker = ({ onChange }) => {
  const [popoverActive, setPopoverActive] = useState(false);
  const [type, setType] = useState(status.All);
  const togglePopoverActive = useCallback(
    () => setPopoverActive((popoverActive) => !popoverActive),
    []
  );

  const items = useMemo(() => {
    const names = Object.entries(STATUS_LABELS);
    return names.slice(0, names.length).map((item) => ({
      content: item[1],
      active: item[0] == type ? true : false,
      onAction: () => {
        togglePopoverActive();
        onChange(item[0]);
        setType(item[0]);
      },
    }));
  }, [type]);

  const popoverActivator = (
    <div className={styles.SortingPicker__button}>
      <Button onClick={togglePopoverActive} icon={OrdersMajor}>
        {STATUS_LABELS[type]}
      </Button>
    </div>
  );

  return (
    <Popover
      active={popoverActive}
      activator={popoverActivator}
      onClose={togglePopoverActive}
    >
      <ActionList items={items} />
    </Popover>
  );
};

export default SortingStatusPicker;
