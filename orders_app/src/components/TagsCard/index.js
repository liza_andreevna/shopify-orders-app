import React from "react";
import { Card, Tag } from "@shopify/polaris";

const NoteCard = ({ tags }) => {
  return (
    <Card title="Tags" sectioned>
      {tags.length !== 0 ? (
        tags.map((tag) => {
          <Tag>{tag}</Tag>;
        })
      ) : (
        <p>No tags from customer</p>
      )}
    </Card>
  );
};

export default NoteCard;
