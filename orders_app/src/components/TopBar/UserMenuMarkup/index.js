import React from "react";
import { TopBar } from "@shopify/polaris";
import styles from "./styles.module.scss";
import ProfileSkeleton from "../../ProfileSkeleton";

const UserMenuMarkup = ({ name, detail, loading }) => {
  if (loading) return <ProfileSkeleton />;
  return (
    <div className={styles.UserMenuWrapper}>
      <TopBar.UserMenu
        name={`${name}`}
        detail={detail}
        initials={`${name}`.slice(0, 1)}
        avatar={"/images/avatar.png"}
      />
    </div>
  );
};

export default UserMenuMarkup;
