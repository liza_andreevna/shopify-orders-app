import React from "react";
import { TopBar } from "@shopify/polaris";
import UserMenuMarkup from "./UserMenuMarkup";
import style from "./styles.module.scss";
import { useQuery } from "@apollo/client";
import { GET_SHOPIFY_SHOP } from "../../graphql/queries/getShopData.schema";

const TopBarMarkup = ({ toggleMobileNavigationActive }) => {
  const { data, loading } = useQuery(GET_SHOPIFY_SHOP);

  return (
    !loading && (
      <div className={style.wrapperTopBar}>
        <TopBar
          showNavigationToggle
          onNavigationToggle={toggleMobileNavigationActive}
          userMenu={
            <UserMenuMarkup
              loading={loading}
              name={data?.shop.name}
              detail={data?.shop.myshopifyDomain}
            />
          }
        />
      </div>
    )
  );
};

export default TopBarMarkup;
