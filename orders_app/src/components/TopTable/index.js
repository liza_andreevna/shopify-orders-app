import React from "react";
import { Card, DataTable, Heading, Spinner } from "@shopify/polaris";
import style from "./styles.module.scss";

const TopTable = ({ rows, loading }) => {
  return (
    <div className={style.tableWrapper}>
      <Heading>Top 5 Orders</Heading>
      <Card>
        {loading ? (
          <div className={style.spinnerWrapper}>
            <Spinner accessibilityLabel="Spinner example" size="large" />
          </div>
        ) : (
          <DataTable
            columnContentTypes={["text", "text"]}
            headings={["Order Name", "Total Price"]}
            rows={rows}
          />
        )}
      </Card>
    </div>
  );
};

export default TopTable;
