export const status = {
  All: "ALL",
  Pending: "PENDING",
  Authorized: "AUTHORIZED",
  Paid: "PAID",
  PartiallyPaid: "PARTIALLY_PAID",
  Refunded: "REFUNDED",
  PartiallyRefunded: "PARTIALLY_REFUNDED",
  Voided: "VOIDED",
};

export const STATUS_COLORS = {
  [status.Pending]: "#D2BF12",
  [status.Authorized]: "#2A94B6",
  [status.Paid]: "#2AB68F",
  [status.PartiallyPaid]: "#C12BDA",
  [status.Refunded]: "#DA2B2B",
  [status.PartiallyRefunded]: "#DB6868",
  [status.Voided]: "#8e030b",
};

export const STATUS_LABELS = {
  [status.All]: "All",
  [status.Pending]: "Pending",
  [status.Authorized]: "Authorized",
  [status.Paid]: "Paid",
  [status.PartiallyPaid]: "Partially Paid",
  [status.Refunded]: "Refunded",
  [status.PartiallyRefunded]: "Partially Refunded",
  [status.Voided]: "Voided",
};

export const STATUS_BADGE = {
  [status.Pending]: "incomplete",
  [status.Authorized]: "complete",
  [status.Paid]: "complete",
  [status.PartiallyPaid]: "partiallyComplete",
  [status.Refunded]: "Refunded",
  [status.PartiallyRefunded]: "partiallyComplete",
  [status.Voided]: "incomplete",
};

export const fulfillmentStatus = {
  Fulfilled: "FULFILLED",
  InProgress: "IN_PROGRESS",
  OnHold: "ON_HOLD",
  Open: "OPEN",
  PartiallyFulfilled: "PARTIALLY_FULFILLED",
  PendingFulfilled: "PENDING_FULFILLMENT",
  Restocked: "RESTOCKED",
  Scheduled: "SCHEDULED",
  Unfulfilled: "UNFULFILLED",
};

export const FUL_LABELS_STATUS = {
  [fulfillmentStatus.Fulfilled]: "Fulfilled",
  [fulfillmentStatus.Unfulfilled]: "Unfulfilled",
  [fulfillmentStatus.InProgress]: "In Progress",
  [fulfillmentStatus.OnHold]: "On Hold",
  [fulfillmentStatus.Open]: "Open",
  [fulfillmentStatus.PartiallyFulfilled]: "Partially Fulfilled",
  [fulfillmentStatus.PendingFulfilled]: "Pending Fulfilled",
  [fulfillmentStatus.Restocked]: "Restocked",
  [fulfillmentStatus.Scheduled]: "Scheduled",
};

export const FUL_STATUS_BADGE = {
  [fulfillmentStatus.Fulfilled]: "complete",
  [fulfillmentStatus.Unfulfilled]: "incomplete",
  [fulfillmentStatus.InProgress]: "partiallyComplete",
  [fulfillmentStatus.OnHold]: "incomplete",
  [fulfillmentStatus.Open]: "incomplete",
  [fulfillmentStatus.PartiallyFulfilled]: "partiallyComplete",
  [fulfillmentStatus.PendingFulfilled]: "partiallyComplete",
  [fulfillmentStatus.Restocked]: "incomplete",
  [fulfillmentStatus.Scheduled]: "incomplete",
};

export const ORDERS_LIMIT = 10;
