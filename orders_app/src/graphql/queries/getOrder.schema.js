import { gql } from "@apollo/client";

export const GET_ORDER = gql`
  query GetOrder($id: ID!) {
    order(id: $id) {
      id
      name
      displayFinancialStatus
      displayFulfillmentStatus
      createdAt
      note
      tags
      customer {
        displayName
        id
        phone
        email
      }
      taxLines {
        title
        ratePercentage
        priceSet {
          presentmentMoney {
            amount
            currencyCode
          }
        }
      }
      transactions(first: 10) {
        id
        amountSet {
          presentmentMoney {
            amount
            currencyCode
          }
        }
      }
      shippingLine {
        title
        originalPriceSet {
          presentmentMoney {
            amount
            currencyCode
          }
        }
      }
      currentSubtotalLineItemsQuantity
      currentSubtotalPriceSet {
        presentmentMoney {
          amount
          currencyCode
        }
      }
      currentTotalPriceSet {
        presentmentMoney {
          amount
          currencyCode
        }
      }
      totalPriceSet {
        presentmentMoney {
          amount
          currencyCode
        }
      }
      shippingAddress {
        zip
        city
        country
        name
        address1
      }
      lineItems(first: 10) {
        edges {
          node {
            id
            quantity
            product {
              id
            }
            image {
              url
            }
            title
            originalUnitPriceSet {
              presentmentMoney {
                amount
                currencyCode
              }
            }
            originalTotalSet {
              presentmentMoney {
                amount
                currencyCode
              }
            }
          }
        }
      }
    }
  }
`;
