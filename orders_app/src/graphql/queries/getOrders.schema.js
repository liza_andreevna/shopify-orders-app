import { gql } from "@apollo/client";

export const GET_ORDERS = gql`
  query GetOrders(
    $first: Int
    $last: Int
    $after: String
    $before: String
    $query: String
  ) {
    orders(
      first: $first
      last: $last
      after: $after
      before: $before
      query: $query
    ) {
      pageInfo {
        hasNextPage
        hasPreviousPage
      }
      edges {
        cursor
        node {
          id
          name
          currencyCode
          totalPriceSet {
            shopMoney {
              amount
            }
          }
          shippingAddress {
            zip
            city
            country
            name
            address1
          }
          lineItems(first: 10) {
            edges {
              node {
                quantity
              }
            }
          }
          createdAt
          displayFinancialStatus
        }
      }
    }
  }
`;
