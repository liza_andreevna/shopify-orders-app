import { gql } from "@apollo/client";

export const GET_SHOPIFY_SHOP = gql`
  query {
    shop {
      id
      myshopifyDomain
      name
      email
      url
    }
  }
`;
