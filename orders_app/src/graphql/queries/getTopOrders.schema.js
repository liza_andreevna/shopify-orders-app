import { gql } from "@apollo/client";

export const GET_TOP_ORDERS = gql`
  query GetTopOrders($query: String) {
    orders(first: 5, sortKey: TOTAL_PRICE, query: $query) {
      edges {
        node {
          id
          name
          currencyCode
          totalPriceSet {
            shopMoney {
              amount
            }
          }
        }
      }
    }
  }
`;
