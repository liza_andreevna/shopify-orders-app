const Cut = (data) => {
  if (data?.edges) {
    return data.edges.map((item) => item.node);
  }
  return [];
};

export default Cut;
