import axios from "axios";

export const fetchAction = async (params) => {
  try {
    const data = await axios.get("/api/statistic", { params: params });
    return data;
  } catch (error) {
    console.log(error);
  }
};
