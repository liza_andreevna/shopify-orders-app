import Link from "next/link";
import { formatter } from "./formatter";
import { format, parseISO } from "date-fns";
import styles from "../templates/Orders/styles.module.scss";
import { STATUS_COLORS, STATUS_LABELS } from "../constants";
import React from "react";
import { useRouter } from "next/router";

export const formatOrdersData = (orders) => {
  const router = useRouter();
  const { shop, host } = router.query;
  const rows = orders.map((order) => [
    <Link href={`/order/${order.id.substr(20)}?shop=${shop}&host=${host}`}>
      {order.name}
    </Link>,
    `${formatter.format(order.totalPriceSet.shopMoney.amount)} ${
      order.currencyCode
    }`,
    `${order.lineItems.edges.length}`,
    `${order.shippingAddress.zip} ${order.shippingAddress.country} ${order.shippingAddress.city} ${order.shippingAddress.name} ${order.shippingAddress.address1}`,
    `${format(parseISO(order.createdAt), "MMM dd, h:mm aaa")}`,
    <span
      className={styles.OrdersTable__status}
      style={{
        backgroundColor: STATUS_COLORS[order.displayFinancialStatus] || "red",
      }}
    >
      {STATUS_LABELS[order.displayFinancialStatus]}
    </span>,
  ]);
  return rows;
};
