import Link from "next/link";
import { formatter } from "./formatter";
import React from "react";
import { useRouter } from "next/router";

export const formatTopOrdersData = (orders) => {
  const router = useRouter();
  const { shop, host } = router.query;
  const rows = orders.map((order) => [
    <Link href={`/order/${order.id.substr(20)}?shop=${shop}&host=${host}`}>
      {order.name}
    </Link>,
    `${formatter.format(order.totalPriceSet.shopMoney.amount)}`,
  ]);
  return rows;
};
