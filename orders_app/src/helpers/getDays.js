import { format, setMonth } from "date-fns";
import { getDaysArray } from "./getDaysArray";

export const getDays = () => {
  let date = new Date();
  let lastDay = format(date, "yyyy-MM-dd");
  const month = setMonth(new Date(lastDay), date.getMonth() - 1);
  let firstDay = format(month, "yyyy-MM-dd");
  const dayList = getDaysArray(new Date(firstDay), new Date(lastDay));
  const result = dayList.map((v) => format(v, "yyyy-MM-dd"));
  return result;
};
