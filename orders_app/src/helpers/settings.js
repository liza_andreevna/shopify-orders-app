const theme = {
  logo: {
    width: 122,
    topBarSource: "/images/logo.svg",
    url: "/",
    accessibilityLabel: "Order App",
  },
};

export default theme;
