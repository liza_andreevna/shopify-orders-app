export function sortData(rows, index, direction) {
  return rows.sort((rowA, rowB) => {
    const amountA =
      index == 4 ? new Date(rowA[index]) : parseFloat(rowA[index].slice(1));
    const amountB =
      index == 4 ? new Date(rowB[index]) : parseFloat(rowB[index].slice(1));
    return direction === "descending" ? amountB - amountA : amountA - amountB;
  });
}
