import React, { useCallback, useState } from "react";
import TopBarMarkup from "../../components/TopBar";
import NavigationMarkup from "../../components/Navigation";
import { Frame, Page } from "@shopify/polaris";
import styles from "./styles.module.scss";

const DefaultLayout = ({ children }) => {
  const [mobileNavigationActive, setMobileNavigationActive] = useState(false);
  const toggleMobileNavigationActive = useCallback(
    () =>
      setMobileNavigationActive(
        (mobileNavigationActive) => !mobileNavigationActive
      ),
    []
  );
  const topBarMarkup = (
    <TopBarMarkup toggleMobileNavigationActive={toggleMobileNavigationActive} />
  );
  return (
    <Frame
      topBar={topBarMarkup}
      navigation={<NavigationMarkup />}
      showMobileNavigation={mobileNavigationActive}
      onNavigationDismiss={toggleMobileNavigationActive}
    >
      <div className={styles.PageWrapper}>
        <Page>{children}</Page>
      </div>
    </Frame>
  );
};

export default DefaultLayout;
