import React, { useState } from "react";
import DefaultLayout from "../../layouts/DefaultLayout";
import TopTable from "../../components/TopTable";
import MainTitle from "../../components/MainTitle";
import ChartView from "../../components/Chart";
import { Layout, Banner } from "@shopify/polaris";
import { useQuery } from "@apollo/client";
import { GET_TOP_ORDERS } from "../../graphql/queries/getTopOrders.schema";
import Cut from "../../helpers/cut";
import { formatTopOrdersData } from "../../helpers/formatTopOrdersData";
import { addDays, formatISO, setMonth } from "date-fns";

const Home = () => {
  const [currentDate, setCurrentDate] = useState({
    date: {
      start: setMonth(new Date(), new Date().getMonth() - 1),
      end: new Date(),
    },
  });
  const { data, loading, error } = useQuery(GET_TOP_ORDERS, {
    variables: {
      query: `created_at:>${formatISO(
        currentDate.date.start
      )} AND created_at:<${formatISO(addDays(currentDate.date.end, 1))}`,
    },
  });
  const orders = Cut(data?.orders);
  const rows = formatTopOrdersData(orders);
  if (error) {
    return (
      <Banner title="Something went wrong" status="critical">
        <p>{error}</p>
      </Banner>
    );
  }
  return (
    <DefaultLayout>
      <Layout>
        <Layout.Section fullWidth>
          <MainTitle />
        </Layout.Section>
        <Layout.Section fullWidth>
          <ChartView title={"Orders"} type="line" />
        </Layout.Section>
        <Layout.Section fullWidth>
          <TopTable rows={rows} loading={loading} />
        </Layout.Section>
      </Layout>
    </DefaultLayout>
  );
};

export default Home;
