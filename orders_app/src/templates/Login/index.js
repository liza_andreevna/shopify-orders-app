import React from "react";
import styles from "./styles.module.scss";
import Image from "next/image";
import LoginForm from "../../components/LoginForm";

const Login = () => {
  return (
    <div className={styles.LoginWrapper}>
      <Image
        src="/images/main-logo.svg"
        alt="Orders App"
        width={451}
        height={69}
      />
      <LoginForm />
    </div>
  );
};

export default Login;
