import React from "react";
import DefaultLayout from "../../layouts/DefaultLayout";
import {
  DisplayText,
  Layout,
  Button,
  TextStyle,
  Banner,
} from "@shopify/polaris";
import { ViewMajor } from "@shopify/polaris-icons";
import { useRouter } from "next/router";
import { useQuery } from "@apollo/client";
import { GET_ORDER } from "../../graphql/queries/getOrder.schema";
import PaymentBadge from "../../components/PaymentBadge";
import FulfillmentBadge from "../../components/FulfillmentBadge";
import styles from "./styles.module.scss";
import { format, parseISO } from "date-fns";
import FulStatusCard from "../../components/FulStatusCard";
import NoteCard from "../../components/NoteCard";
import TagsCard from "../../components/TagsCard";
import CustomerCard from "../../components/CustomerCard";
import PaidCard from "../../components/PaidCard";

const Order = () => {
  const router = useRouter();
  const { data, loading, error } = useQuery(GET_ORDER, {
    variables: {
      id: `gid://shopify/Order/${router.query.id}`,
    },
  });
  if (error) {
    return (
      <Banner title="Something went wrong" status="critical">
        <p>{error}</p>
      </Banner>
    );
  }
  return (
    <DefaultLayout>
      {!loading && (
        <Layout>
          <Layout.Section fullWidth>
            <div className={styles.orderHeader}>
              <div className={styles.titleWrapper}>
                <DisplayText size={"extraLarge"}>
                  Order {data?.order?.name}
                </DisplayText>
                <PaymentBadge status={data?.order?.displayFinancialStatus} />
                <FulfillmentBadge
                  status={data?.order?.displayFulfillmentStatus}
                />
              </div>
              <Button
                icon={ViewMajor}
                onClick={() =>
                  router.push(
                    `https://${router.query.shop}/admin/orders/${router.query.id}`
                  )
                }
              >
                View
              </Button>
            </div>
            <TextStyle variation="subdued">
              {format(parseISO(data?.order?.createdAt), "PPPp")}
            </TextStyle>
          </Layout.Section>
          <Layout.Section>
            <FulStatusCard order={data?.order} router={router} />
            <PaidCard order={data?.order} />
          </Layout.Section>
          <Layout.Section secondary>
            <NoteCard note={data?.order?.note} />
            <CustomerCard
              customer={data?.order?.customer}
              shippingAddress={data?.order?.shippingAddress}
              router={router}
            />
            <TagsCard tags={data?.order?.tags} />
          </Layout.Section>
        </Layout>
      )}
    </DefaultLayout>
  );
};

export default Order;
