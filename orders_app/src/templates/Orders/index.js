import React, { useEffect, useState, useMemo } from "react";
import DefaultLayout from "../../layouts/DefaultLayout";
import {
  DisplayText,
  TextStyle,
  Layout,
  Spinner,
  Card,
  Banner,
} from "@shopify/polaris";
import SearchFilters from "../../components/Filters";
import OrdersTable from "../../components/OrdersTable";
import { useQuery } from "@apollo/client";
import { GET_ORDERS } from "../../graphql/queries/getOrders.schema";
import Cut from "../../helpers/cut";
import { format, formatISO, setMonth, addDays } from "date-fns";
import styles from "./styles.module.scss";
import { formatOrdersData } from "../../helpers/formatOrdersData";
import { ORDERS_LIMIT, status } from "../../constants";
import OrdersPagination from "../../components/OrdersTablePagination";

const Orders = () => {
  const [page, setPage] = useState(1);
  const [load, setLoad] = useState({
    left: false,
    right: false,
  });

  const [filters, setFilters] = useState({
    search: "",
    status: status.All,
    date: {
      start: setMonth(new Date(), new Date().getMonth() - 1),
      end: new Date(),
    },
  });

  const { data, loading, fetchMore, error, refetch } = useQuery(GET_ORDERS, {
    variables: {
      first: ORDERS_LIMIT,
      query: `created_at:>${formatISO(
        filters.date.start
      )} AND created_at:<${formatISO(addDays(filters.date.end, 1))}`,
    },
  });

  useEffect(() => {
    setPage(1);
  }, [filters]);

  useEffect(() => {
    fetchMore({
      variables: {
        query: `financial_status:${filters.status.toLowerCase()}, created_at:>${formatISO(
          filters.date.start
        )} AND created_at:<${formatISO(addDays(filters.date.end, 1))}`,
      },
    });
  }, [filters.date]);

  useEffect(() => {
    let search = filters.search !== "" ? `name:${filters.search}` : "";
    refetch({
      query: `financial_status:${filters.status.toLowerCase()}, ${search}, created_at:>${formatISO(
        filters.date.start
      )} AND created_at:<${formatISO(addDays(filters.date.end, 1))}`,
    });
  }, [filters.status]);

  useEffect(() => {
    refetch({
      query:
        filters.search !== ""
          ? `financial_status:${filters.status.toLowerCase()}, name:${
              filters.search
            }, created_at:>${formatISO(
              filters.date.start
            )} AND created_at:<${formatISO(addDays(filters.date.end, 1))}`
          : `financial_status:${filters.status.toLowerCase()}, created_at:>${formatISO(
              filters.date.start
            )} AND created_at:<${formatISO(addDays(filters.date.end, 1))}`,
    });
  }, [filters.search]);

  const memoizedCallback = useMemo(() => {
    if (data !== undefined) {
      return page !== 1
        ? (page - 1) * 10 + data.orders.edges.length
        : page * data.orders.edges.length;
    }
  }, [data]);

  const ordersData = data?.orders;
  const orders = Cut(ordersData);
  const rows = formatOrdersData(orders);

  const onSearch = (value) => {
    setFilters((current) => ({
      ...current,
      search: value,
    }));
  };

  const onSortingStatus = (value) => {
    setFilters((current) => ({
      ...current,
      status: value,
    }));
  };

  const onDateFilter = (value) => {
    setFilters((current) => ({
      ...current,
      date: value,
    }));
  };

  const onPaginationNext = () => {
    setLoad((current) => ({
      ...current,
      right: true,
    }));
    fetchMore({
      variables: {
        first: ORDERS_LIMIT,
        query: `financial_status:${filters.status.toLowerCase()}, created_at:>${formatISO(
          filters.date.start
        )} AND created_at:<${formatISO(addDays(filters.date.end, 1))}`,
        after: ordersData.edges[ordersData.edges.length - 1].cursor,
      },
      updateQuery: (previousResult, { fetchMoreResult }) => {
        if (!fetchMoreResult) return previousResult;
        const newOrders = fetchMoreResult.orders.edges;
        const pageInfo = fetchMoreResult.orders.pageInfo;
        const newCursor = newOrders[newOrders.length - 1].cursor;
        return {
          cursor: newCursor,
          orders: {
            pageInfo,
            edges: newOrders,
          },
        };
      },
    }).then((res) => {
      if (res) {
        setLoad((current) => ({
          ...current,
          right: false,
        }));
      }
    });
    setPage((current) => current + 1);
  };

  const onPaginationPrev = () => {
    setLoad((current) => ({
      ...current,
      left: true,
    }));
    fetchMore({
      query: GET_ORDERS,
      variables: {
        last: ORDERS_LIMIT,
        query: `financial_status:${filters.status.toLowerCase()}, created_at:>${formatISO(
          filters.date.start
        )} AND created_at:<${formatISO(addDays(filters.date.end, 1))}`,
        before: ordersData.edges[0].cursor,
      },
      updateQuery: (previousResult, { fetchMoreResult }) => {
        if (!fetchMoreResult) return previousResult;
        const newOrders = fetchMoreResult.orders.edges;
        const pageInfo = fetchMoreResult.orders.pageInfo;
        const newCursor = newOrders[0].cursor;
        return {
          cursor: newCursor,
          orders: {
            pageInfo,
            edges: newOrders,
          },
        };
      },
    }).then((res) => {
      if (res) {
        setLoad((current) => ({
          ...current,
          left: false,
        }));
      }
    });
    setPage((current) => (current > 1 ? current - 1 : current));
  };

  if (error) {
    return (
      <Banner title="Something went wrong" status="critical">
        <p>{error}</p>
      </Banner>
    );
  }

  return (
    <DefaultLayout>
      <Layout>
        <Layout.Section fullWidth>
          <div className={styles.TextWrapper}>
            <DisplayText size={"extraLarge"}>Orders</DisplayText>
            <TextStyle variation="subdued">Track your orders</TextStyle>
          </div>
        </Layout.Section>
        <Layout.Section fullWidth>
          <SearchFilters
            onSearch={onSearch}
            onSortingStatus={onSortingStatus}
            onDateFilter={onDateFilter}
            selectedDates={filters.date}
          />
          <div className={styles.cardWrapper}>
            <Card>
              {loading ? (
                <div className={styles.loading}>
                  <Spinner size="large" />
                </div>
              ) : (
                <div className={styles.mainContent}>
                  <OrdersTable initiallySortedRows={rows} />
                  <OrdersPagination
                    createdAtMin={format(filters.date.start, "yyyy-MM-dd")}
                    createdAtMax={format(
                      addDays(filters.date.end, 1),
                      "yyyy-MM-dd"
                    )}
                    hasNext={ordersData.pageInfo.hasNextPage}
                    onPaginationNext={onPaginationNext}
                    onPaginationPrev={onPaginationPrev}
                    hasPrev={ordersData.pageInfo.hasPreviousPage}
                    ordersCount={memoizedCallback}
                    loading={load}
                  />
                </div>
              )}
            </Card>
          </div>
        </Layout.Section>
      </Layout>
    </DefaultLayout>
  );
};

export default Orders;
